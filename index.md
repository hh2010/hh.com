---
layout: default
---

Welcome to my personal homepage.  The purpose of this site is to demonstrate
some of the projects I am working on as I revive a longtime interest in computer
science.  Many of the projects will be through my participation in the <a href="http://www.thisismetis.com"><b>Metis</b></a> data science bootcamp.

This site was built using the [**Jekyll**](https://jekyllrb.com/)
framework and based on the [**Jekyll-Mono**](https://github.com/AkshayAgarwal007/Jekyll-Mono)
theme created by Akshay Agarwal.
