---
layout: page
title: My Projects
permalink: /projects/
---

### OpenCV Card Detector
As part of one of our three required "investigations" for Metis, I created a
program using OpenCV for Python that would recognize the image of a "card" from
playing online poker, and return a value in text format that represented the card
(i.e. holding Ace of hearts and Ace of diamonds would return "Ah, Ad").

You can find the Jupyter notebook [here](https://github.com/hh2010/metis_hasan/blob/master/inv/card_detect/card_detect.ipynb).

You can find my blog post on the project [here](/opencv).

***

### No Limit Texas Hold 'Em Solver
I have built a heads up No Limit Hold 'Em solver in Python based on Will Tipton's video series ["Solving Poker with IPython and Fictitious Play."](http://www.husng.com/content/will-tipton-hunl-video-pack-2-0)

Currently the program solves for Expected Value of game
trees according to the Fictitious Play algorithm.  There is also a very basic
GUI functionality for equity calculations.  I am working on optimizing the code
and making the GUI fully functional.

You can find the GitHub repo for the project [here](https://github.com/hh2010/hunl).

***

### Poker Site Collusion Detector - Hand History Parser (private)
This project consisted of multiple Python scripts which were used to parse a directory
full of hand history files from an online poker site and create an SQL database with
several important details about the hand and players in question.

The goal of the project is to identify potential collusion between players. The
next step in the project is to build classification algorithms to detect potential
deviations from expected behavior.

If you are interested in learning more about this project, you can [contact me](mailto:hasan.haq@gmail.com) and I will consider providing access to the [Bitbucket](https://bitbucket.org) repo.
