---
layout: page
title: About
permalink: /about/
---

## General
* Six years corporate finance experience (investment banking and private equity)
* Currently a student at [Metis](http://www.thisismetis.com)
    * Metis is a 12-week data science bootcamp
    * Work on projects using Python and machine learning
* Currently pursuing opportunities in data science

## Education

* Bachelors in Economics from Northwestern University (Evanston, IL) - 2010
* Bartlett High School (Bartlett, IL) - 2006

## Work History

* Finance Associate, Bravo Natural Resources
* Investment Banking Analyst, Goldman Sachs
* Investment Banking Analyst, Scotiabank

## Other

* Previous Board Roles:
    * Board Member, Young Professionals in Energy (Tulsa)
    * Director of Corporate Relations, Young Professionals in Energy (Houston)
    * Board Member, Muslim Urban Professionals (Houston)
